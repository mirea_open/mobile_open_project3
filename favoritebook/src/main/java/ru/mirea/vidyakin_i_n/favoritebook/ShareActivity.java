package ru.mirea.vidyakin_i_n.favoritebook;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class ShareActivity extends AppCompatActivity {
    EditText edtxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        edtxt = findViewById(R.id.editTextTextPersonName);
    }

    public void onClickSetBook(View view) {
        Intent data = new Intent();
        data.putExtra(MainActivity.USER_MESSAGE, edtxt.getText().toString());
        setResult(Activity.RESULT_OK, data);
        finish();
    }
}