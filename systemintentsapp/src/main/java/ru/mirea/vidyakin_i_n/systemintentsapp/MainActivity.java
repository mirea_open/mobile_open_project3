package ru.mirea.vidyakin_i_n.systemintentsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickCall(View view) {
        Intent intentcall = new Intent(Intent.ACTION_DIAL);
        intentcall.setData(Uri.parse("tel:89811112233"));
        startActivity(intentcall);
    }

    public void onClickOpenBrowser(View view) {
        Intent intentbrowse = new Intent(Intent.ACTION_VIEW);
        intentbrowse.setData(Uri.parse("http://developer.android.com"));
        startActivity(intentbrowse);
    }

    public void onClickOpenMaps(View view) {
        Intent intentmap = new Intent(Intent.ACTION_VIEW);
        intentmap.setData(Uri.parse("geo:55.74479,37.613944"));
        startActivity(intentmap);
    }
}